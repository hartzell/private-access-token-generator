# Private Access Token Generator

This application provides a simple way to create a personal access token
for a GitLab CI Job. 

The use cases are various

* Create/Update [GitLab Badges](https://docs.gitlab.com/ee/api/project_badges.html)
* Push Commit/Tag to Repository from CI Job
* [Download Artifacts](https://docs.gitlab.com/ee/api/jobs.html#get-job-artifacts) (Job Token not available in core/starter)
* Don't tie Tokens to Users! All tokens (be it private, deploy, trigger) are user-bound and will stop working when a user stops being a user. 
* Create/Delete Project Triggers

#### Example Links

```yaml
token_gen:
  image: everpeace/curl-jq
  script:
  - 'export CI_PERSONAL_TOKEN=$(curl -s https://<address-of-generator>/?jwt=${CI_JOB_JWT} | jq .token)'
  - 'echo $CI_PERSONAL_TOKEN'
  - 'sleep 5'
```

* [Impersonation Tokens Registry](https://gitlab.demo.i2p.online/admin/users/tpoffenbarger-demo/impersonation_tokens)
* [Sample CI Pipelines](https://gitlab.demo.i2p.online/gitlab-ultimate/tpoffenbarger/authored-ci/pipelines)
* [GitLab CI YAML](https://gitlab.demo.i2p.online/gitlab-ultimate/tpoffenbarger/authored-ci/blob/master/.gitlab-ci.yml#L22-24)

#### Requirements

* This application creates Impersonation Tokens and must have a Private Access Token of a GitLab Administrator
* This application must have access to run `docker inspect` and `docker ps`


#### Some Development at GitLab That Could Render this Useless

* [Identity API](https://gitlab.com/gitlab-org/gitlab/issues/24123)
* [Make Pipeline Permissions more Controllable/Flexible](https://gitlab.com/gitlab-org/gitlab/issues/35067)
* [Allow Runners to Push via their CI Token](https://gitlab.com/gitlab-org/gitlab-foss/issues/63858): deprecated project
